<?php

use Illuminate\Database\Seeder;
use App\Acid;

class AcidsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Acid::truncate();

        Acid::create(['name' => 'HCL', 'description' => 'Strong Acid']);
        Acid::create(['name' => 'CH3COOH', 'description' => 'Weak Acid']);
    }
}
