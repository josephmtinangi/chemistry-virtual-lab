<?php

use Illuminate\Database\Seeder;
use App\Base;

class BasesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Base::truncate();

        Base::create(['name' => 'NaOH', 'description' => 'Strong base']);
        Base::create(['name' => 'NH3', 'description' => 'Weak base']);
        Base::create(['name' => 'Na2CO3', 'description' => 'Strong base']);

    }
}
