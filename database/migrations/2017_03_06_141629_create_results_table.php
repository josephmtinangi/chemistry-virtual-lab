<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('assignment_id')->unsigned();
            $table->double('vo1')->nullable();
            $table->double('v11')->nullable();
            $table->double('v1')->nullable();
            $table->double('vo2')->nullable();
            $table->double('v12')->nullable();
            $table->double('v2')->nullable();
            $table->double('vo3')->nullable();
            $table->double('v13')->nullable();
            $table->double('v3')->nullable();
            $table->integer('acid_id')->unsigned();
            $table->integer('base_id')->unsigned();
            $table->string('initial_color')->nullable();
            $table->string('final_color')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('results');
    }
}
