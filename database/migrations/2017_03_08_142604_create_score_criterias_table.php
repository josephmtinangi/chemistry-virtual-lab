<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScoreCriteriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('score_criterias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('acid_id')->unsigned();
            $table->integer('base_id')->unsigned();
            $table->string('initial_color');
            $table->string('final_color');
            $table->integer('volume');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('score_criterias');
    }
}
