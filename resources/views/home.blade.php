@extends('layouts.app')

@section('page_title', 'Home')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2">
            @include('partials.sidebar')
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading text-center"><i class="fa fa-dashboard"></i> Overview</div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="text-center">
                                <i class="fa fa-users fa-5x"></i>
                                <h1>{{ $students }}</h1>
                                <h2>Students</h2>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="text-center">
                                <i class="fa fa-flask fa-5x"></i>
                                <h1>{{ $assignments }}</h1>
                                <h2>Experiments</h2>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="text-center">
                                <i class="fa fa-pencil fa-5x"></i>
                                <h1>{{ $results }}</h1>
                                <h2>Submits</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
