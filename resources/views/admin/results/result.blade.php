@if(Auth::user()->is_admin)
    <h3>Student Details</h3>

    <table class="table table-bordered table-hover">
        <tbody>
        <tr>
            <th>Admission number</th>
            <td colspan="3">{{ $result->user->username }}</td>
            <td>{{ $result->user->student->gender }}</td>
        </tr>
        <tr>
            <th>Full name</th>
            <td colspan="4">
                {{ $result->user->student->first_name }}
                {{ $result->user->student->middle_name }}
                {{ $result->user->student->last_name }}
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <th>Class</th>
            <td>{{ $result->user->student->class }}</td>
            <th>Year</th>
            <td>{{ $result->user->student->year }}</td>
        </tr>
        </tbody>
    </table>
@endif


<h3>TITRATION READINGS</h3>


<table class="table table-hover">
    <tbody>
    <tr>
        <th>TITRATION READING (cm<sup>3</sup>)</th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th></th>
    </tr>
    <tr>
        <td>Final burrete reading</td>
        <td>{{ $result->vo1 }}</td>
        <td>{{ $result->vo2 }}</td>
        <td>{{ $result->vo3 }}</td>
        <td></td>
    </tr>
    <tr>
        <td>Initial burrete reading</td>
        <td>{{ $result->v11 }}</td>
        <td>{{ $result->v12 }}</td>
        <td>{{ $result->v13 }}</td>
        <td></td>
    </tr>
    <tr>
        <td>Volume of acid used</td>
        <td>
            {{ $result->v1 }}
        </td>
        <td>
            {{ $result->v2 }}
        </td>
        <td>
            {{ $result->v3 }}
        </td>
        <td></td>
    </tr>
    <tr>
        <td>&nbsp</td>
        <td>&nbsp</td>
        <td>&nbsp</td>
        <td>&nbsp</td>
        <td>&nbsp</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <th>ACID</th>
        <th>BASE</th>
        <th>INITIAL COLOR</th>
        <th>FINAL COLOR</th>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>{{ $result->acid->name }}</td>
        <td>{{ $result->base->name }}</td>
        <td>{{ $result->initial_color }}</td>
        <td>{{ $result->final_color }}</td>
    </tr>
    </tbody>
</table>