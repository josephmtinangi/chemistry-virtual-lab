@extends('layouts.app')

@section('page_title', 'Results')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading text-center"><i class="fa fa-file-excel-o"></i> All Results</div>

                    <div class="panel-body">

                        @if(count($results) > 0)
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Student Admissin #</th>
                                    <th>Experiment</th>
                                    <th>View Results</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; ?>
                                @foreach($results as $result)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>
                                            {{ $result->student->username }}
                                        </td>
                                        <td>
                                            {{ $result->assignment->name }}
                                        </td>
                                        <td>
                                            <a href="{{ url('admin/results/'.$result->id) }}"><i class="fa fa-link"></i>
                                                View</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="text-center">
                                <i class="fa fa-file-excel-o fa-5x"></i>
                                <h1>No submits yet</h1>
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
