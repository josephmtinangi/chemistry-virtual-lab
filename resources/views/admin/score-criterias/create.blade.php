@extends('layouts.app')

@section('page_title', 'Home')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2">

            @include('partials.sidebar')

        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading text-center"><i class="fa fa-bar-chart"></i> Scoring Criteria</div>

                <div class="panel-body">
                    
                    <form action="{{ url('admin/score-criterias') }}" method="POST" class="form-horizontal" role="form">

                            {{ csrf_field() }}
                            

                            <div class="form-group">
                                <label for="acid_id" class="col-sm-2 control-label">Acid</label>
                                <div class="col-sm-10">

                                    @if(count($acids) > 0)
                                        @foreach($acids as $acid)
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="acid_id" id="acid_id" value="{{ $acid->id }}" required="required">
                                                    {{ $acid->name }}
                                                </label>
                                            </div>
                                        @endforeach
                                    @else
                                        <div class="alert alert-info">
                                            No chemical is defined in the database
                                        </div>
                                    @endif 

                                </div>    
                            </div>

                            <div class="form-group">
                                <label for="base_id" class="col-sm-2 control-label">Base</label>
                                <div class="col-sm-10">
                                    @if(count($bases) > 0)
                                        @foreach($bases as $base)
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="base_id" id="base_id" value="{{ $base->id }}" required="required">
                                                    {{ $base->name }}
                                                </label>
                                            </div>
                                        @endforeach
                                    @else
                                        <div class="alert alert-info">
                                            No chemical is defined in the database
                                        </div>
                                    @endif
                                </div>    
                            </div>
                    
                            <div class="form-group">
                                <label for="initial_color" class="col-sm-2 control-label">Initial Color</label>
                                <div class="col-sm-5">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="initial_color" id="initial_color" value="Red">
                                            Red
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="initial_color" id="initial_color" value="Yellow">
                                            Yellow
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="initial_color" id="initial_color" value="Pink">
                                            Pink
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="initial_color" id="initial_color" value="Colorless">
                                            Colorless
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="final_color" class="col-sm-2 control-label">Final Color</label>
                                <div class="col-sm-5">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="final_color" id="final_color" value="Red">
                                            Red
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="final_color" id="final_color" value="Yellow">
                                            Yellow
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="final_color" id="final_color" value="Pink">
                                            Pink
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="final_color" id="final_color" value="Colorless">
                                            Colorless
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="volume" class="col-sm-2 control-label">Volume</label>
                                <div class="col-sm-5">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="volume" id="volume" value="1">
                                            Same as acid
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="volume" id="volume" value="2">
                                            Twice the acid
                                        </label>
                                    </div>
                                </div>
                            </div>                           
                    
                            <div class="form-group">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <button type="submit" class="btn btn-primary">Create</button>
                                </div>
                            </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
