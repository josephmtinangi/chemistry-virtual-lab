@extends('layouts.app')

@section('page_title', 'Home')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">

                @include('partials.sidebar')

            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading text-center"><i class="fa fa-bar-chart"></i> Scoring Criteria</div>

                    <div class="panel-body">

                        @if(count($score_criterias) > 0)
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Acid</th>
                                    <th>Base</th>
                                    <th>Initial Color</th>
                                    <th>Final Color</th>
                                    <th>Added</th>
                                    <th colspan="2" class="text-center"><i class="fa fa-cogs"></i></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($score_criterias as $score_criteria)
                                    <tr>
                                        <td>{{ $score_criteria->id }}</td>
                                        <td>{{ $score_criteria->acid->name }}</td>
                                        <td>{{ $score_criteria->base->name }}</td>
                                        <td>{{ $score_criteria->initial_color }}</td>
                                        <td>{{ $score_criteria->final_color }}</td>
                                        <td>{{ $score_criteria->created_at }}</td>
                                        <td>
                                            <a href="#"><i class="fa fa-edit"></i> Edit</a>
                                        </td>
                                        <td>
                                            <a href="#" class="text-danger"><i class="fa fa-trash"></i> Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif

                        <hr>

                        <a href="{{ url('admin/score-criterias/create') }}" class="btn btn-primary">Add</a>

                        <hr>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
