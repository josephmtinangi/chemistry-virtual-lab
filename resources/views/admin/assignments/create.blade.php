@extends('layouts.app')

@section('page_title', 'Create a new assignment')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading text-center"><i class="fa fa-flask"></i> New Assignment</div>

                    <div class="panel-body">

                        <form action="{{ url('admin/assignments') }}" method="POST" class="form-horizontal" role="form">

                            {{ csrf_field() }}

                            @include('admin.assignments._form', ['btn_text' => 'Add'])

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
