@extends('layouts.app')

@section('page_title', 'Create a new assignment')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading text-center"><i class="fa fa-flask"></i> Edit {{ $assignment->name }}
                    </div>

                    <div class="panel-body">

                        <form action="/admin/assignments/{{ $assignment->id }}" method="POST" class="form-horizontal"
                              role="form">

                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}

                            @include('admin.assignments._form', ['btn_text' => 'Update'])

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
