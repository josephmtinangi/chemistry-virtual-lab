@extends('layouts.app')

@section('page_title', 'Assignments')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading text-center"><i class="fa fa-flask"></i> Assignments</div>

                    <div class="panel-body">
                        @if(count($assignments) > 0)
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Name</th>
                                    <th>Added</th>
                                    <th>Updated</th>
                                    <th>Submits</th>
                                    <th colspan="2" class="text-center"><i class="fa fa-cogs"></i></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1 ?>
                                @foreach($assignments as $assignment)
                                    <tr>
                                        <td>{{ $i++ }}.</td>
                                        <td>{{ $assignment->name }}</td>
                                        <td>{{ $assignment->created_at->toFormattedDateString() }}</td>
                                        <td>{{ $assignment->updated_at->toFormattedDateString() }}</td>
                                        <td>{{ $assignment->students()->count() }}</td>
                                        <td><a class="btn btn-primary"
                                               href="{{ url('admin/assignments/'.$assignment->id.'/edit') }}"><i
                                                        class="fa fa-edit"></i> Edit</a></td>
                                        <td>
                                            <a class="btn btn-danger btn-sm" data-toggle="modal"
                                               href='#{{ $assignment->id }}'><i
                                                        class="fa fa-trash"></i> Delete</a>
                                            <div class="modal fade" id="{{ $assignment->id }}">
                                                <div class="modal-dialog">

                                                    <form method="POST"
                                                          action="/admin/assignments/{{ $assignment->id }}">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}

                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title">{{ $assignment->name }}</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                Are you sure that you want to delete
                                                                <strong>{{ $assignment->name }}</strong>?
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default"
                                                                        data-dismiss="modal"><i
                                                                            class="fa fa-remove"></i> Cancel
                                                                </button>
                                                                <button type="submit" class="btn btn-danger"><i
                                                                            class="fa fa-trash"></i> Delete
                                                                </button>
                                                            </div>
                                                        </div>

                                                    </form>

                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="text-center">
                                <div class="alert alert-info">
                                    You have not created any assignment yet.
                                </div>
                            </div>
                        @endif

                        <a href="{{ url('admin/assignments/create') }}" class="btn btn-primary">Add New</a>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
