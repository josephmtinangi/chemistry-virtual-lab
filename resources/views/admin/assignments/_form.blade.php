<div class="form-group">
    <label for="name" class="col-sm-2 control-label">Name</label>
    <div class="col-sm-10">
        <input type="text"
               name="name"
               id="name"
               value="{{ isset($assignment->name) ? $assignment->name : old('name') }}"
               class="form-control"
               placeholder="What is the name of your experiment?">
    </div>
</div>

<div class="form-group">
    <label for="acid_id" class="col-sm-2 control-label">Acid</label>
    <div class="col-sm-10">

        @if(count($acids) > 0)
            @foreach($acids as $acid)
                <div class="radio">
                    <label>
                        <input type="radio"
                               name="acid_id"
                               id="acid_id"
                               value="{{ $acid->id }}"
                               required="required"
                                {{ isset($assignment->acid_id) && $assignment->acid_id == $acid->id ? ' checked' : '' }}
                        >
                        {{ $acid->name }}
                    </label>
                </div>
            @endforeach
        @else
            <div class="alert alert-info">
                No chemical is defined in the database
            </div>
        @endif

    </div>
</div>

<div class="form-group">
    <label for="base_id" class="col-sm-2 control-label">Base</label>
    <div class="col-sm-10">
        @if(count($bases) > 0)
            @foreach($bases as $base)
                <div class="radio">
                    <label>
                        <input type="radio"
                               name="base_id"
                               id="base_id"
                               value="{{ $base->id }}"
                               required="required"
                                {{ isset($assignment->base_id) && $assignment->base_id == $base->id ? ' checked' : '' }}
                        >
                        {{ $base->name }}
                    </label>
                </div>
            @endforeach
        @else
            <div class="alert alert-info">
                No chemical is defined in the database
            </div>
        @endif
    </div>
</div>

<div class="form-group">
    <label for="va" class="col-sm-2 control-label">Volume of acid</label>
    <div class="col-sm-10">
        <input type="number" name="va" id="va" value="{{ isset($assignment->va) ? $assignment->va : '' }}"
               class="form-control"
               placeholder="What is the volume of acid to be used?">
    </div>
</div>

<div class="form-group">
    <label for="description" class="col-sm-2 control-label">Description</label>
    <div class="col-sm-10">
        <textarea name="description" id="description" rows="10" class="form-control"
                  placeholder="Any additional information about this experiment.">{{ isset($assignment->description) ? $assignment->description : '' }}</textarea>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
        <button type="submit" class="btn btn-primary">{{ $btn_text }}</button>
    </div>
</div>