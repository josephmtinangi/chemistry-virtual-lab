@extends('layouts.app')

@section('page_title', 'Students')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading text-center"><i class="fa fa-users"></i> Students</div>

                    <div class="panel-body">
                        @if(count($students) > 0)
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>First name</th>
                                    <th>Middle name</th>
                                    <th>Last name</th>
                                    <th>Admission #</th>
                                    <th>Gender</th>
                                    <th>Class</th>
                                    <th>Year</th>
                                    <th colspan="2" class="text-center"><i class="fa fa-cogs"></i></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; ?>
                                @foreach($students as $student)
                                    <tr>
                                        <td>{{ $i++ }}.</td>
                                        <td>{{ $student->first_name }}</td>
                                        <td>{{ $student->middle_name }}</td>
                                        <td>{{ $student->last_name }}</td>
                                        <td>{{ $student->user->username }}</td>
                                        <td>{{ $student->gender }}</td>
                                        <td>{{ $student->class }}</td>
                                        <td>{{ $student->year }}</td>
                                        <td>
                                            <a href="{{ url('admin/students/'.$student->id.'/edit') }}"
                                               class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                        </td>
                                        <td>
                                            <a class="btn btn-danger btn-sm" data-toggle="modal"
                                               href='#{{ $student->id }}'><i
                                                        class="fa fa-trash"></i> Delete</a>
                                            <div class="modal fade" id="{{ $student->id }}">
                                                <div class="modal-dialog">
                                                    <form method="POST" action="/admin/students/{{ $student->id }}">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}

                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title">
                                                                    Delete {{ $student->first_name }} {{ $student->middle_name }} {{ $student->last_name }}</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                Are you sure?
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default"
                                                                        data-dismiss="modal"><i
                                                                            class="fa fa-remove"></i> Cancel
                                                                </button>
                                                                <button type="submit" class="btn btn-danger"><i
                                                                            class="fa fa-trash"></i>
                                                                    Delete
                                                                </button>
                                                            </div>
                                                        </div>

                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="text-center">
                                <div class="alert alert-info">
                                    No students
                                </div>
                            </div>
                        @endif

                        <a href="{{ url('admin/students/create') }}" class="btn btn-primary">Add New</a>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
