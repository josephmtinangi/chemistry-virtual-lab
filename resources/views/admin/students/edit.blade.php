@extends('layouts.app')

@section('page_title', 'Edit ' . $student->first_name)

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading text-center">
                        Edit {{ $student->first_name }} {{ $student->middle_name }} {{  $student->last_name }}</div>

                    <div class="panel-body">

                        @include('errors.list')

                        <form action="{{ url('admin/students/'.$student->id) }}" method="POST" class="form-horizontal"
                              role="form">

                            {{ csrf_field() }}

                            {{ method_field('patch') }}

                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <h3>Admission number: {{ $student->user->username }}</h3>
                                </div>
                            </div>

                            @include('admin.students._form', ['btn_text' => 'Update'])

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
