<div class="form-group">
    <label for="first_name" class="col-sm-3 control-label">First name</label>
    <div class="col-sm-5">
        <input type="text"
               name="first_name"
               id="first_name"
               value="{{ isset($student->first_name) ? $student->first_name : old('first_name') }}"
               class="form-control">
    </div>
</div>

<div class="form-group">
    <label for="middle_name" class="col-sm-3 control-label">Middle name
        (Option)</label>
    <div class="col-sm-5">
        <input type="text"
               name="middle_name"
               id="middle_name"
               value="{{ isset($student->middle_name) ? $student->middle_name : old('middle_name') }}"
               class="form-control">
    </div>
</div>

<div class="form-group">
    <label for="last_name" class="col-sm-3 control-label">Last name</label>
    <div class="col-sm-5">
        <input type="text"
               name="last_name"
               id="last_name"
               value="{{ isset($student->last_name) ? $student->last_name : old('last_name') }}"
               class="form-control">
    </div>
</div>

<div class="form-group">
    <label for="gender" class="col-sm-3 control-label">Gender</label>
    <div class="col-sm-5">
        <div class="radio">
            <label>
                <input type="radio" name="gender" id="gender"
                       value="F" {{ isset($student->gender) && $student->gender == 'F' ? ' checked' : '' }}>
                Female
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="gender" id="gender"
                       value="M" {{ isset($student->gender) && $student->gender == 'M' ? ' checked' : '' }}>
                Male
            </label>
        </div>
    </div>
</div>

<div class="form-group">
    <label for="class" class="col-sm-3 control-label">Class</label>
    <div class="col-sm-5">
        <select name="class" id="class" class="form-control">
            <option selected="selected" disabled="disabled">-Select-</option>
            <option value="Form I" {{ isset($student->class) && $student->class == 'Form I' ? 'selected' : '' }}>Form
                I
            </option>
            <option value="Form II" {{ isset($student->class) && $student->class == 'Form II' ? 'selected' : '' }}>Form
                II
            </option>
            <option value="Form III" {{ isset($student->class) && $student->class == 'Form III' ? 'selected' : '' }}>
                Form III
            </option>
            <option value="Form IV" {{ isset($student->class) && $student->class == 'Form IV' ? 'selected' : '' }}>Form
                IV
            </option>
        </select>
    </div>
</div>

<div class="form-group">
    <label for="year" class="col-sm-3 control-label">Year</label>
    <div class="col-sm-5">
        <select name="year" id="class" class="form-control">
            <option selected="selected" disabled="disabled">-Select-</option>
            @for($i = 2016; $i<= date('Y'); $i++)
                <option value="{{ $i }}" {{ isset($student->year) && $student->year == $i ? ' selected' : '' }}>{{ $i }}</option>
            @endfor
        </select>
    </div>
</div>


<div class="form-group">
    <div class="col-sm-6 col-sm-offset-3">
        <button type="submit" class="btn btn-primary">{{ $btn_text }}</button>
    </div>
</div>