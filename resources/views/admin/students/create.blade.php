@extends('layouts.app')

@section('page_title', 'New Student(s)')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading text-center">New Student(s)</div>

                    <div class="panel-body">

                        @include('errors.list')

                        <form action="{{ url('admin/students') }}" method="POST" class="form-horizontal"
                              role="form">

                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="username" class="col-sm-3 control-label">Admission number</label>
                                <div class="col-sm-5">
                                    <input type="number"
                                           name="username"
                                           id="username"
                                           value="{{ old('username') }}"
                                           class="form-control">
                                </div>
                            </div>

                            @include('admin.students._form', ['btn_text' => 'Add'])

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
