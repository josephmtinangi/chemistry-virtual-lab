@extends('layouts.app')

@section('page_title', 'Help')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="page-header">
                    <h1>Help
                        <small> Usage Instructions</small>
                    </h1>
                </div>

                <table class="table table-bordered table-hover">
                    <tbody>
                    <tr>
                        <th>
                            Add Students
                        </th>
                    </tr>
                    <tr>
                        <td>
                            Step 1: Click Students <br>

                            Step 2: Click add new button <br>

                            Step 3: Fill the required information <br>

                            Step 4: Click add
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Edit Student
                        </th>
                    </tr>
                    <tr>
                        <td>
                            Step 1: Click Students <br>

                            Step 2: Click the Edit button on the student you want to edit <br>

                            Step 3: Change the necessary information <br>

                            Step 4: Click Update
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Delete Student
                        </th>
                    </tr>
                    <tr>
                        <td>
                            Step 1: Click Students <br>

                            Step 2: Click the Delete button on the student you want to delete <br>

                            Step 3: You will be presented with a confirmation dialog <br>

                            Step 4: Click Delete
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Add Assignments
                        </th>
                    </tr>
                    <tr>
                        <td>
                            Step 1: Click Assignments <br>

                            Step 2: Click the Add New button <br>

                            Step 3: Fill the required information <br>

                            Step 4: Click Add
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Edit Assignment
                        </th>
                    </tr>
                    <tr>
                        <td>
                            Step 1: Click Assignments <br>

                            Step 2: Click the Edit button on the assignment you want to edit <br>

                            Step 3: Change the necessary information <br>

                            Step 4: Click Update
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Delete Assignment
                        </th>
                    </tr>
                    <tr>
                        <td>
                            Step 1: Click Assignments <br>

                            Step 2: Click the Delete button on the assignment you want to delete <br>

                            Step 3: You will be presented with a confirmation dialog <br>

                            Step 4: Click Delete
                        </td>
                    </tr>
                    <tr>
                        <th>
                            View Results
                        </th>
                    </tr>
                    <tr>
                        <td>
                            Click Submits <br>

                            Choose the one you want <br>

                            Click View
                        </td>
                    </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
@endsection
