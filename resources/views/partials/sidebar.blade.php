<div class="list-group">
    <a href="{{ url('admin') }}" class="list-group-item{{ Request::is('admin') == 'admin' ? ' active' : '' }}"><i
                class="fa fa-dashboard fa-fw"></i> Overview</a>
    <a href="{{ url('admin/students') }}"
       class="list-group-item{{ Request::is('admin/students') == 'admin/students' ? ' active' : '' }}"><i
                class="fa fa-user fa-fw"></i> Students</a>
    <a href="{{ url('admin/assignments') }}"
       class="list-group-item{{ Request::is('admin/assignments') == 'admin/assignments' ? ' active' : '' }}"><i
                class="fa fa-flask fa-fw"></i> Assignments</a>
    <a href="{{ url('admin/results') }}" class="list-group-item{{ Request::is('admin/results') == 'admin/results' ? ' active' : '' }}"><i
                class="fa fa-file-excel-o fa-fw"></i> Submits</a>
    {{-- <a href="{{ url('admin/score-criterias') }}" class="list-group-item"><i class="fa fa-shield fa-fw"></i> Score Criterias</a> --}}
    {{-- <a href="#" class="list-group-item{{ Request::is('admin/settings') == 'admin/settings' ? ' active' : '' }}"><i
                class="fa fa-cog fa-fw"></i> Settings</a> --}}
</div>






