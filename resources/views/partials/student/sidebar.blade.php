<div class="list-group">
	<a href="/" class="list-group-item"><i class="fa fa-home"></i> Home</a>
    <a href="{{ url('/') }}" class="list-group-item{{ Request::is('/') == '/' ? ' active' : '' }}"><i
                class="fa fa-flask"></i> Experiments</a>
    {{-- <a href="{{ url('assignments') }}"
       class="list-group-item{{ Request::is('assignments') == 'assignments' ? ' active' : '' }}"><i
                class="fa fa-flask fa-fw"></i> Experiments</a> --}}
    {{-- <a href="{{ url('results') }}" class="list-group-item{{ Request::is('results') == 'results' ? ' active' : '' }}"><i
                class="fa fa-file-excel-o fa-fw"></i> My Results</a> --}}
    <a href="{{ url('settings/change-password') }}" class="list-group-item{{ Request::is('settings/change-password') == 'settings/change-password' ? ' active' : '' }}"><i
                class="fa fa-lock fa-fw"></i> Change Password</a>                
</div>