@extends('layouts.app')

@section('page_title', $assignment->name)

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('partials.student.sidebar')
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading text-center"><i class="fa fa-flask"></i> {{ $assignment->name }}</div>

                    <div class="panel-body">

                        {{ $assignment->description }}

                        <hr>

                        <dl class="dl-horizontal">
                            <dt>Acid</dt>
                            <dd>{{ $assignment->acid->name }}</dd>
                            <dt>Base</dt>
                            <dd>{{ $assignment->base->name }}</dd>
                        </dl>

                        <hr>

                        <dl class="dl-horizontal">
                            <dt>Issue date</dt>
                            <dd>{{ $assignment->created_at }}</dd>
                            <dt>Submission date</dt>
                            <dd>{{ $assignment->submission_date }}</dd>
                            <dt>&nbsp;</dt>
                            <dd></dd>
                            <dt>&nbsp;</dt>
                            <dd>
                                @if($assignment->students->count() > 0)

                                    @foreach($assignment->students as $student)
                                        @if($student->id == $assignment->user_id)
                                            Yes
                                        @else
                                            <a href="/assignments/{{ $assignment->id }}/attempt"
                                               class="btn btn-primary"><i class="fa fa-flash"></i> Attempt</a>
                                        @endif
                                    @endforeach

                                @else
                                    <a href="/assignments/{{ $assignment->id }}/attempt" class="btn btn-primary"><i
                                                class="fa fa-flash"></i> Attempt</a>
                                @endif
                            </dd>
                        </dl>

                    </div>


                </div>
            </div>
        </div>
    </div>
@endsection
