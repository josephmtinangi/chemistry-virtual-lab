@extends('layouts.app')

@section('page_title', $assignment->name)

@section('content')

    @include('student.assignments.workspace')

    @include('errors.list');

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4">
                <a class="btn btn-primary btn-lg" data-toggle="modal" href='#modal-id'>Fill Experiment Data</a>
                <div class="modal fade" id="modal-id">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"
                                        aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Data collected for {{ $assignment->name }}</h4>
                            </div>
                            <div class="modal-body">

                                <form action="{{ url('results') }}" method="POST">

                                    {{ csrf_field() }}

                                    <input type="hidden" name="assignment_id" value="{{ $assignment->id }}">

                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Please record your results carefully and submit</h3>
                                        </div>
                                        <div class="panel-body">

                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                    <th colspan="4">PRACTICAL RESULTS</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <th>TITRATION READINGS (cm<sup>3</sup>)</th>
                                                    <th>1</th>
                                                    <th>2</th>
                                                    <th>3</th>
                                                </tr>
                                                <tr>
                                                    <th><label for="vo1">Final burrete reading</label></th>
                                                    <td>
                                                        <input type="text"
                                                               name="vo1"
                                                               id="vo1"
                                                               class="form-control"
                                                               placeholder="00.0"
                                                               maxlength="5" >
                                                    </td>
                                                    <td>
                                                        <input type="text"
                                                               name="vo2"
                                                               id="vo2" 
                                                               class="form-control" 
                                                               placeholder="00.0"
                                                               maxlength="5" >
                                                    </td>
                                                    <td>
                                                        <input type="text"
                                                               name="vo3"
                                                               id="vo3"
                                                               class="form-control"
                                                               placeholder="00.0"
                                                               maxlength="5" >
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Initial burrete reading</th>
                                                    <td>
                                                        <input type="text" name="v11" id="v11" class="form-control"
                                                               placeholder="00.0"
                                                               maxlength="5" >
                                                    </td>
                                                    <td>
                                                        <input type="text" name="v12" id="v12" class="form-control"
                                                               placeholder="00.0"
                                                               maxlength="5" >
                                                    </td>
                                                    <td>
                                                        <input type="text" name="v13" id="v13" class="form-control"
                                                               placeholder="00.0"
                                                               maxlength="5" >
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Volume of acid used</th>
                                                    <td>
                                                        <input type="text" name="v1" id="v1" class="form-control"
                                                               placeholder="00.0"
                                                               maxlength="5" >
                                                    </td>
                                                    <td>
                                                        <input type="text" name="v2" id="v2" class="form-control"
                                                               placeholder="00.0"
                                                               maxlength="5" >
                                                    </td>
                                                    <td>
                                                        <input type="text" name="v3" id="v3" class="form-control"
                                                               placeholder="00.0"
                                                               maxlength="5" >
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>

                                    <br>

                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Chemicals and Color change</h3>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <h4>ACID</h4>

                                                    @if(count($acids) > 0)
                                                        @foreach($acids as $acid)
                                                            <div class="radio">
                                                                <label>
                                                                    <input type="radio" name="acid_id" id="acid_id" value="{{ $acid->id }}" >
                                                                    {{ $acid->name }}
                                                                </label>
                                                            </div>
                                                        @endforeach
                                                    @else
                                                        <div class="alert alert-info">
                                                            No chemical is defined in the database
                                                        </div>
                                                    @endif 

                                                </div>
                                                <div class="col-sm-4">
                                                    <h4>BASE</h4>
                                                    
                                                    @if(count($bases) > 0)
                                                        @foreach($bases as $base)
                                                            <div class="radio">
                                                                <label>
                                                                    <input type="radio" name="base_id" id="base_id" value="{{ $base->id }}" >
                                                                    {{ $base->name }}
                                                                </label>
                                                            </div>
                                                        @endforeach
                                                    @else
                                                        <div class="alert alert-info">
                                                            No chemical is defined in the database
                                                        </div>
                                                    @endif


                                                </div>
                                                <div class="col-sm-4">
                                                    <h4>COLOR</h4>
                                                    <div class="form-group">
                                                        <label for="">Initial</label>
                                                        <input type="text" name="initial_color" id="initial_color"
                                                               class="form-control" >
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Final</label>
                                                        <input type="text" name="final_color" id="final_color"
                                                               class="form-control" >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="text-right">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>

                                </form>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
@endsection
