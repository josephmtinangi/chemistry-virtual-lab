@extends('layouts.app')

@section('page_title', 'My Experiments')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('partials.student.sidebar')
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading text-center"><i class="fa fa-flask"></i> Experiments</div>

                    <div class="panel-body">
                        @if(count($assignments) > 0)
                            @foreach($assignments->chunk(2) as $assignmentSet)
                                <div class="row">
                                    @foreach($assignmentSet as $assignment)
                                        <div class="col-sm-6">
                                            <div class="thumbnail">
                                                <img data-src="#" alt="">
                                                <div class="caption">
                                                    <h3><i class="fa fa-flask"></i>
                                                        <a href="{{ url('assignments/'.$assignment->id) }}">{{ $assignment->name }}</a>
                                                    </h3>
                                                    <p>
                                                        {{ str_limit($assignment->description, 75) }}
                                                        {{-- <a href="{{ url('assignments/'.$assignment->id) }}" class="btn btn-link">Read more &raquo;</a> --}}
                                                    </p>
                                                    <p>
                                                        <strong>Added: </strong> {{ $assignment->created_at->toFormattedDateString() }}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                        @else
                            <div class="text-center">
                                <i class="fa fa-battery-0 fa-5x"></i>
                                <h1>No experiments at the moment!</h1>
                                <h2>Just chill out.</h2>
                            </div>
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
