@extends('layouts.app')

@section('page_title', 'Home')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('partials.student.sidebar')
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading text-center"><i class="fa fa-bar-chart"></i> Overview</div>

                    <div class="panel-body">

                        <h2 class="text-center">{{ $value_now }}% of experiments
                            completed</h2>
                        <div class="progress progress-striped">
                            <div class="progress-bar" role="progressbar"
                                 aria-valuenow="{{ $value_now }}"
                                 aria-valuemin="0" aria-valuemax="100"
                                 style="width: {{ $value_now }}%;">
                                <span class="sr-only">{{ $value_now }}
                                    % Complete</span>
                            </div>
                        </div>                        

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
