@extends('layouts.app')

@section('page_title', $result->assignment->name)

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">

                @if(Auth::user()->is_admin)
                    @include('partials.sidebar')
                @else
                    @include('partials.student.sidebar')
                @endif

            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading text-center"><i
                                class="fa fa-file-excel-o"></i> {{ $result->assignment->name }}</div>

                    <div class="panel-body">

                        @include('admin.results.result')

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
