@extends('layouts.app')

@section('page_title', 'My Results')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                @include('partials.student.sidebar')
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading text-center"><i class="fa fa-file-excel-o"></i> My Results</div>

                    <div class="panel-body">

                        @if(count($results) > 0)
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Name</th>
                                    <th>Submitted</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($results as $result)
                                    <tr>
                                        <td>{{ $result->id }}</td>
                                        <td>
                                            <a href="{{ url('results/'.$result->id) }}">{{ $result->assignment->name }}</a>
                                        </td>
                                        <td>{{ $result->created_at }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="text-center">
                                <i class="fa fa-battery-0 fa-5x"></i>
                                <h1>You have not attempted any experiment yet.</h1>
                                <a href="{{ url('assignments') }}" class="btn btn-primary"><i class="fa fa-flask"></i>
                                    Attempt</a>
                            </div>
                        @endif

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
