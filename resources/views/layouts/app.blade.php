<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('page_title')</title>

    <!-- Fonts -->
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css">

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
<div id="app">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                    {{-- <li><a href="{{ url('assignments') }}">Experiments</a></li> --}}
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        {{-- <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li> --}}
                    @else
                        @if(Auth::user()->is_admin)
                            <li><a href="{{ url('admin/help') }}"><i class="fa fa-question-circle"></i> Help</a></li>
                            <li><a href="{{ url('admin') }}"><i class="fa fa-wrench"></i> Dashboard</a></li>
                        @endif
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false">
                                <i class="fa fa-user"></i> {{ Auth::user()->username }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="/settings/change-password"><i class="fa fa-lock fa-fw"></i> Change Password</a>
                                </li>
                                <li>
                                    <a href="{{ url('/logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="fa fa-sign-out fa-fw"></i> Logout
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        <div class="col-sm-4 col-sm-offset-4">
            @include('flash::message')
        </div>
    </div>

    @yield('content')
</div>

<footer>
    <p class="text-center">
        {{ Config('app.name') }}
    </p>
    <p class="text-center">
        Copyright &copy; {{ date('Y') }}
    </p>
    <p class="text-center">
        Designed and Developed by the <a href="http://udom.ac.tz">University of Dodoma</a>
    </p>
    <p class="text-center">
        <a href="http://cive.hakikidawa.org">College of Informatics and Virtual Education</a>
    </p>
</footer>

<!-- Scripts -->
<script src="/vendor/js/jquery.min.js"></script>
<script src="/vendor/js/bootstrap.min.js"></script>
</body>
</html>
