@extends('layouts.app')

@section('page_title', 'Home')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2">

            @if(Auth::user()->is_admin)
                @include('partials.sidebar')
            @else
                @include('partials.student.sidebar')
            @endif
            
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-lock"></i> Change Password</div>

                <div class="panel-body">

                    @include('errors.list')
                    
                    <form action="{{ url('settings/change-password') }}" method="POST" class="form-horizontal" role="form">

                            {{ csrf_field() }}
                            
                            <div class="form-group">
                                <label for="old_password" class="col-sm-2 control-label">Old Password</label>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <input type="password" name="old_password" id="old_password" class="form-control" placeholder="Your Old Password">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password" class="col-sm-2 control-label">New Password</label>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <input type="password" name="password" id="password" class="form-control" placeholder="New Password">
                                    </div>
                                </div>
                            </div>                           

                            <div class="form-group">
                                <label for="password_confirmation" class="col-sm-2 control-label">Old Password</label>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Password Confirmation">
                                    </div>
                                </div>
                            </div>
                    
                            
                    
                            <div class="form-group">
                                <div class="col-sm-5 col-sm-offset-2">
                                    <button type="submit" class="btn btn-primary btn-block">Change</button>
                                </div>
                            </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
