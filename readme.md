# Chemistry Virtual Laboratory

Practice Chemistry Experiments Online.

Demo is found at  [http://damp-fortress-86055.herokuapp.com](http://damp-fortress-86055.herokuapp.com)

## Installation

How to install

### Software needed

- Linux Operating System

  Recommended (Ubuntu 16.04)
  
- Git

- PHP >= 5.6.4

- Web server (Apache or Nginx)

  Recommended (Nginx)
  
- Database (MySQL, MariaDB, Sqlite3, Postgres)

  Recommended (MySQL)
  
- Composer

- Node (With Yarn, PM2, Bower, Grunt and Gulp)

- Redis

- Memcached

- Beanstalkd

### Step by step instructions

**Step 1:** Clone this repository into your web server root directory

**Step 2:** Run `composer install`

**Step 3** Change server document root to point to public directory

**Step 4** Run `php artisan db:seed`

**Step 5** You are ready to go.

## Credentials

**Admin**

*Username:* admin

*Password:* secret

**NB:** After login, change the password before doing anything

**Student**

As an admin, you can create students. A student can login using his/her admission number as username and his/her last name as password.

## How to

### Add Students

Step 1: Click Students

Step 2: Click add new button

Step 3: Fill the required information

Step 4: Click add

## Edit Student

Step 1: Click Students

Step 2: Click the Edit button on the student you want to edit

Step 3: Change the necessary information

Step 4: Click Update

## Delete Student

Step 1: Click Students

Step 2: Click the Delete button on the student you want to delete

Step 3: You will be presented with a confirmation dialog

Step 4: Click Delete


### Add Assignments

Step 1: Click Assignments

Step 2: Click the Add New button

Step 3: Fill the required information

Step 4: Click Add

### Edit Assignment

Step 1: Click Assignments

Step 2: Click the Edit button on the assignment you want to edit

Step 3: Change the necessary information

Step 4: Click Update

### Delete Assignment

Step 1: Click Assignments

Step 2: Click the Delete button on the assignment you want to delete

Step 3: You will be presented with a confirmation dialog

Step 4: Click Delete

### View Results

- Click Submits

- Choose the one you want

- Click View
