<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScoreCriteria extends Model
{
    protected $fillable = [
    	'acid_id',
    	'base_id',
    	'initial_color',
    	'final_color',
    	'volume',
    ];

    public function acid()
    {
        return $this->belongsTo(Acid::class);
    }

    public function base()
    {
        return $this->belongsTo(Base::class);
    }
}
