<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Acid extends Model
{
    protected $fillable = ['name', 'description'];
}
