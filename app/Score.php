<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    protected $fillable = [
    	'user_id',
    	'assignment_id',
        'v1',
        'v2',
        'v3',
    	'score',
    	'score_criteria_id',
    ];
}
