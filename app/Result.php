<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $fillable = [
        'user_id',
        'assignment_id',
        'vo1',
        'v11',
        'v1',
        'vo2',
        'v12',
        'v2',
        'vo3',
        'v13',
        'v3',
        'acid_id',
        'base_id',
        'initial_color',
        'final_color',
    ];

    public function assignment()
    {
        return $this->belongsTo(Assignment::class, 'assignment_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function acid()
    {
        return $this->belongsTo(Acid::class);
    }

    public function base()
    {
        return $this->belongsTo(Base::class);
    }
}
