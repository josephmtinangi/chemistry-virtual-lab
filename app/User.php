<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function results()
    {
        return $this->hasMany(Result::class);
    }

    public function student()
    {
        return $this->hasOne(Student::class, 'user_id');
    }

    public function assignments()
    {
        return $this->belongsToMany(Student::class, 'student_assignment', 'assignment_id', 'user_id')->withTimestamps();
    }
}
