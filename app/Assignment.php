<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    protected $fillable = [
        'name',
        'va',
        'description',
        'path',
        'published_at',
        'submission_date',
    ];

    public function getDates()
    {
        return [
            'published_at',
            'submission_date',
            'created_at',
            'updated_at'
        ];
    }

    public function acid()
    {
    	return $this->belongsTo(Acid::class);
    }

    public function base()
    {
    	return $this->belongsTo(Base::class);
    }

    public function students()
    {
        return $this->belongsToMany(User::class, 'student_assignment', 'assignment_id', 'user_id')->withTimestamps();
    }
}
