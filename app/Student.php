<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = ['admission_number', 'first_name', 'middle_name', 'last_name', 'class', 'year', 'user_id'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
