<?php

namespace App\Http\Controllers\Admin;

use App\Assignment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Acid;
use App\Base;

class AssignmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assignments = Assignment::with('students')->latest()->get();
        return view('admin.assignments.index', compact('assignments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $acids = Acid::all();
        $bases = Base::all();

        return view('admin.assignments.create', compact('acids', 'bases'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'acid_id' => 'required',
            'base_id' => 'required',
            'va' => 'required',
        ]);

        $assignment = new Assignment();
        $assignment->name = $request->input('name');
        $assignment->acid_id = $request->input('acid_id');
        $assignment->base_id = $request->input('base_id');
        $assignment->va = $request->input('va');
        $assignment->description = $request->input('description');
        $assignment->save();

        flash('Assignment added successfully.', 'success');

        return redirect('admin/assignments');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Assignment $assignment
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit(Assignment $assignment)
    {
        $acids = Acid::all();
        $bases = Base::all();

        return view('admin.assignments.edit', compact('assignment', 'acids', 'bases'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'acid_id' => 'required',
            'base_id' => 'required',
            'va' => 'required',
        ]);

        $assignment = new Assignment();
        $assignment->name = $request->input('name');
        $assignment->acid_id = $request->input('acid_id');
        $assignment->base_id = $request->input('base_id');
        $assignment->va = $request->input('va');
        $assignment->description = $request->input('description');
        $assignment->save();

        flash('Assignment added successfully.', 'success');

        return redirect('admin/assignments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Assignment $assignment)
    {
        $assignment->delete();

        flash('Assignment deleted successfully.', 'success');

        return back();
    }
}
