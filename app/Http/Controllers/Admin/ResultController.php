<?php

namespace App\Http\Controllers\Admin;

use App\Result;
use App\Http\Controllers\Controller;
use App\Score;
use App\ScoreCriteria;

class ResultController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $results = Result::with(['user', 'assignment'])->latest()->get();

        return view('admin.results.index', compact('results'));
    }

    public function show($id)
    {
        $result = Result::with(['assignment', 'user', 'acid'])->find($id);

        return view('admin.results.show', compact('result'));
    }
}
