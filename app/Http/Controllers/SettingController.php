<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;

class SettingController extends Controller
{
    public function changePassword()
    {
    	return view('settings.change-password');
    }

    public function storePassword(Request $request)
    {
    	$this->validate($request, [
            'old_password' => 'required',
            'password' => 'required|min:6|confirmed',
    	]);

    	$user = Auth::user();

    	$user->password = bcrypt($request->input('password'));

    	$user->save();

        flash('Password changed successfully', 'success');


    	return redirect('/');
    }
}
