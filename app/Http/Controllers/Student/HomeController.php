<?php

namespace App\Http\Controllers\Student;

use App\Assignment;
use App\Result;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();

        $student_experiments = Result::whereUserId($user->id)->count();
        $total_experiments = Assignment::count();

        $value_now = 0;

        if ($total_experiments != 0) {
            $value_now = ($student_experiments / $total_experiments) * 100;
        }

        return view('student.home', compact('value_now'));
    }
}
