<?php

namespace App\Http\Controllers\Student;

use App\Assignment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Acid;
use App\Base;

class AssignmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $assignments = Assignment::with('students')->latest()->get();

        return view('student.assignments.index', compact('assignments'));
    }

    public function show($id)
    {
        $assignment = Assignment::with('students')->find($id);

        return view('student.assignments.show', compact('assignment'));
    }

    public function attempt($id)
    {
        $assignment = Assignment::find($id);

        $acids = Acid::all();
        $bases = Base::all();

        return view('student.assignments.attempt', compact('assignment', 'acids', 'bases'));
    }
}
