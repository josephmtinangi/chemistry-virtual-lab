<?php

namespace App\Http\Controllers\Student;

use App\Assignment;
use App\ScoreCriteria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Result;
use Auth;
use App\Score;

class ResultController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();
        $results = $user->results()->with(['assignment'])->latest()->get();

        return view('student.results.index', compact('results'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'vo1' => 'required',
            'v11' => 'required',
            'v1' => 'required',
            'vo2' => 'required',
            'v12' => 'required',
            'v2' => 'required',
            'vo3' => 'required',
            'v13' => 'required',
            'v3' => 'required',
            'acid_id' => 'required',
            'base_id' => 'required',
            'initial_color' => 'required',
            'final_color' => 'required',
        ]);

        $user = Auth::user();

        $result = new Result();
        $result->fill($request->all());

        $user->results()->save($result);

        $assignment = Assignment::find($request->input('assignment_id'));
        $user->assignments()->attach($assignment);

        flash('Experiment submitted successfully.', 'success');


        return redirect('/');
    }

    public function show($id)
    {
        $user = Auth::user();
        $result = Result::with(['assignment', 'student', 'acid'])->whereUserId($user->id)->find($id);

        $assignment = Assignment::find($result->assignment_id);
        $score = Score::whereAssignmentId($assignment->id)->whereUserId($user->id)->first();
        $score_criteria = ScoreCriteria::find($score->score_criteria_id);

        return view('student.results.show', compact('result', 'score', 'score_criteria'));
    }

}
