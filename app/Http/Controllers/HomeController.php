<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\Assignment;
use App\Result;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::count();
        $assignments = Assignment::count();
        $results = Result::count();

        return view('home', compact('students', 'assignments', 'results'));
    }

    public function help()
    {
        return view('admin.help');
    }
}
