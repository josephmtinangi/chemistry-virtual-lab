<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Student', 'middleware' => 'auth'], function () {
    // Route::get('/', 'HomeController@index');
    Route::get('/', 'AssignmentController@index');
    Route::get('assignments/{id}', 'AssignmentController@show');
    Route::get('assignments/{id}/attempt', 'AssignmentController@attempt');
    Route::get('results', 'ResultController@index');
    Route::resource('results', 'ResultController');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('admin', 'HomeController@index');
    Route::get('admin/help', 'HomeController@help');
});

Route::group(['namespace' => 'Admin', 'middleware' => 'auth'], function () {
    Route::resource('admin/students', 'StudentController');
    Route::resource('admin/assignments', 'AssignmentController');
    Route::get('admin/results', 'ResultController@index');
    Route::get('admin/results/{id}', 'ResultController@show');
    Route::resource('admin/score-criterias', 'ScoreCriteriaController');
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('settings/change-password', 'SettingController@changePassword');
    Route::post('settings/change-password', 'SettingController@storePassword');
});